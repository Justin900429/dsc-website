const express = require('express');
const app = express();

app.use(express.static('public'));

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/html_files/index.html")
});

app.get("/about", (req, res) => {
  res.sendFile(__dirname + "/html_files/about.html");
});

app.get("/activities", (req, res) => {
  res.sendFile(__dirname + "/html_files/activities.html");
});

app.get("/members", (req, res) => {
  res.sendFile(__dirname + "/html_files/members.html");
});

app.listen(3000, function() {
  console.log("Server stated on port 3000");
});
